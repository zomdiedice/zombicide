package zombice;

import java.util.ArrayList;
import java.util.Random;

public class Hacha extends Arma{
	
	

	public Hacha(String nombre, int dano, int alcance, int acierto) {
		super(nombre,dano,alcance,acierto);
	}
	
	 @Override
	    public void ataqueEspecial(ArrayList<Zombie> zombies) {
	        
	        Random random = new Random();
	        for (int i = 0; i < zombies.size(); i++) {
	            Zombie zombie = zombies.get(i);
	            if (zombie.esGordo()) {
	                System.out.println("¡Ataque especial del Hacha! ¡El Gordo '" + zombie.getNombre() + "' ha sido eliminado!");
	                zombies.remove(i);
	                return;
	            }
	        }
	        
	        System.out.println("¡Ataque especial del Hacha! No hay Gordos para eliminar.");
	    }
}
