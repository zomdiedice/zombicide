package zombice;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Partida {
	public ArrayList<Jugador> jugadores;
	public int nivel;

	public Partida() {
		jugadores = new ArrayList<Jugador>();
	}

	public void setJugadores(ArrayList<Jugador> jugadores) {
		this.jugadores = jugadores;

	}
	public ArrayList<Jugador> getJugadores(ArrayList<Jugador> jugadores) {
		return jugadores;
		
	}

	public ArrayList<Jugador> getJugador() {
		return jugadores;

	}

	public void setNivel(int nivel) {
		this.nivel = nivel;

	}

	public int getNivel() {
		return nivel;

	}

	public void iniciar() {
		System.out.println("¡Comienza la partida en el nivel " + nivel + "!");
	}

	public void agregarJugador(Jugador jugador) {

		jugadores.add(jugador);
	}

	public boolean jugadorMas10() {

		if (jugadores.size() >= 4) {
			System.out.println("No se pueden agregar más personajes, el límite es de 10. ");
			return true;
		}
		return false;

	}


	public ArrayList<Jugador> seleccionarPersonajes() {

		ArrayList<Jugador> jugadoresSeleccionados = new ArrayList<>();
		Scanner leer = new Scanner(System.in);
		int numJugadores = 0;

		// Pedir al usuario que seleccione el número de jugadores
		do {
			System.out.println("Ingrese el número de jugadores (mínimo 3 y maximo 6): ");
			numJugadores = leer.nextInt();
		} while (numJugadores < 3 || numJugadores > 6);

		if (numJugadores == 3) {
			// Si solo hay 3 jugadores disponibles, seleccionar automáticamente
			System.out.println("seleccionar automáticamente:");
			for (int i = jugadores.size() - 1; i >= jugadores.size() - numJugadores; i--) {
				jugadoresSeleccionados.add(jugadores.get(i));
				System.out.println("Ha seleccionado a: " + jugadores.get(i).getNombre());
			}
			return jugadoresSeleccionados; 

		}
		
		
		if (numJugadores > jugadoresSeleccionados.size()) {
		    int jugadoresFaltantes = numJugadores - jugadores.size();
		    for (int i = 0; i < jugadoresFaltantes; i++) {
		        System.out.println("¡¡¡¡No hay suficientes personajes!!!!");
		        System.out.println("¡¡¡¡TENEMOS QUE CREAR MÁSSSS!!!!");
		        System.out.println("Nombre de personaje:");
		        String nombre = leer.next();
		        Arma arma = new Arma();

		        Jugador nuevoJugador = new Jugador(5, 5, nombre, arma);
		        agregarJugador(nuevoJugador);
		    }
		}


		
		System.out.println("*****************************");
		System.out.println("Personajes disponibles:");
		for (int i = 0; i < jugadores.size(); i++) {
			System.out.println((i + 1) + ". " + jugadores.get(i).getNombre());
		}

		// Seleccionar los personajes
		System.out.println("Seleccione los personajes para la partida:");
		for (int i = 0; i < numJugadores; i++) {
			System.out.print("Jugador " + (i + 1) + ": ");
			int seleccion = leer.nextInt();
			if (seleccion >= 1 && seleccion <= jugadores.size()) {
				Jugador jugadorSeleccionado = jugadores.get(seleccion - 1);
				jugadoresSeleccionados.add(jugadorSeleccionado);
				System.out.println("Ha seleccionado a: " + jugadorSeleccionado.getNombre());

				// agregar el jugador seleccionado a la partida
			} else {
				System.out.println("Selección no válida. Inténtelo de nuevo.");
				i--; // Reintentar la selección para este jugador
			}
		}

		
		return jugadoresSeleccionados;

	}

	public void jugarNivel(ArrayList<Jugador> jugadoresSeleccionados) {boolean todosMuertos = false;
    
    ArrayList<Jugador> jugadoresVivos = new ArrayList<>();
  iniciar();
  ArrayList<Zombie> zombies = generarZombiesAleatorios(getNivel());

  Scanner scanner = new Scanner(System.in);
	while (!todosMuertos) {
  
		boolean todosZombiesMuertos = true;
		for (Zombie zombie : zombies) {
		    if (!zombie.estaMuerto()) {
		        todosZombiesMuertos = false;
		        break;
		    }
		}
		if (todosZombiesMuertos) {
		    nivel++;
		    System.out.println("¡Todos los zombies han muerto! ¡Subiendo al nivel " + nivel + "!");
		    zombies = generarZombiesAleatorios(nivel); // Engendra una nueva oleada de zombis
		}

    System.out.println("Generando zombies:");
    for (int i = 0; i < zombies.size(); i++) {
        Zombie zombie = zombies.get(i);
        System.out.println("Tipo: " + zombie.getTipo());
    }

   
   
        for (Jugador jugador : jugadoresSeleccionados) {
            if (!jugador.estaMuerto()) {
                jugadoresVivos.add(jugador);
            }
        }

        System.out.println("\nSeleccione una acción para cada jugador:");

        
        for (Jugador jugador : jugadoresVivos) {
            System.out.println("Turno de " + jugador.getNombre() + ":");
            System.out.println("0) Pasar");
            System.out.println("1) Atacar");
            System.out.println("2) Habilidad Especial");
            System.out.println("3) Buscar");
            System.out.println("4) Cambiar Arma");
            System.out.print("Seleccione una opción: ");
            int opcion;
            while (true) {
                System.out.print("Seleccione una opción: ");
                opcion = scanner.nextInt();

                if (opcion >= 0 && opcion <= 4) {
                    break;
                } else {
                    System.out.println("Opción no válida. Inténtelo de nuevo.");
                }
            }

            switch (opcion) {
                case 0:
                    System.out.println(jugador.getNombre() + " ha pasado su turno.");
                    break;
                case 1:
                    jugador.atacar(zombies);
                    break;
                case 2:
                    jugador.usarHabilidadEspecial(zombies);
                    break;
                case 3:
                    jugador.buscar();
                    break;
                case 4:
                    jugador.cambiarArma();
                    break;
                default:
                    System.out.println("Opción no válida. Inténtelo de nuevo.");
                    break;
            }
        }

        // Los jugadores están muertos, si es así el juego ha terminado
        todosMuertos = true;
        for (Jugador jugador : jugadoresSeleccionados) {
            if (!jugador.estaMuerto()) {
                todosMuertos = false;
                break;
            }
        }
        
        if (todosMuertos) {
            System.out.println("Todos los jugadores han muerto. ¡El juego ha terminado!");
            break; // fin game cc
        }

     

        // Haz que los zombis supervivientes ataquen a un personaje aleatorio una vez finalizadas todas las acciones del jugador
        Random random = new Random();
        for (Zombie zombie : zombies) {
            if (!zombie.estaMuerto()) {
                int movimientosRestantes = zombie.getMovimiento();
                while (movimientosRestantes > 0) {
                    int index = random.nextInt(jugadoresVivos.size());
                    Jugador objetivo = jugadoresVivos.get(index);
                    // atacar jugador
                    objetivo.serAtacadoPorZombie(zombie);
                    movimientosRestantes--;
                }
            }
        }
        // salud restante
        for (Jugador jugador : jugadoresSeleccionados) {
            System.out.println(jugador.getNombre() + " - Salud: " + jugador.getSalud());
        }

        // Muestra información sobre cada jugador
    
        System.out.println("\nInformación de cada jugador antes de la próxima ronda:");
        for (Jugador jugador : jugadoresSeleccionados) {
            if (!jugador.estaMuerto()) {
                System.out.println(jugador.getNombre() + " - Salud: " + jugador.getSalud() + ", Arma equipada: " + jugador.getArmaEquipada().getNombre());
            }
        }
       

    }
}
	  

	private ArrayList<Zombie> generarZombiesAleatorios(int numZombies) {
		ArrayList<Zombie> zombies = new ArrayList<>();
		Random random = new Random();

		for (int i = 0; i < numZombies; i++) {
			int tipo = random.nextInt(3); // 0, 1, 2
			Zombie zombie;
			switch (tipo) {
			case 0:
				zombie = new Gordo();
				break;
			case 1:
				zombie = new Corredor();
				break;
			case 2:
				zombie = new Caminante();
				break;
			default:
				zombie = new Gordo();
			}
			zombies.add(zombie);
		}

		return zombies;
	}
}
