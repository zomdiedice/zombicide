package zombice;

import java.util.Random;

public abstract class Zombie extends Humanoide {
	

	private int movimiento;
	private int dano;
	private String tipo;
	 private boolean activarHabilidadEspecial;
	

	public Zombie(int saludMax, int salud, String nombre, int movimiento,int dano, String tipo) {
		super(saludMax, salud, nombre);

		setMovimiento(movimiento);
		setDano(dano);
		setTipo(tipo);	
		activarHabilidadEspecial = false;
		}

	 public void setActivarHabilidadEspecial(boolean activarHabilidadEspecial) {
	        this.activarHabilidadEspecial = activarHabilidadEspecial;
	    }
	    public boolean isActivarHabilidadEspecial() {
	        return activarHabilidadEspecial;
	    }
	    public void verificarActivacionHabilidadEspecial() {
	        Random random = new Random();
	        int probabilidad = random.nextInt(101); 

	        if (probabilidad >= 95) {
	            activarHabilidadEspecial = true;
	            System.out.println(getNombre() + " ha activado su habilidad especial al morir.");
	        } else {
	            activarHabilidadEspecial = false;
	            System.out.println(getNombre() + " no ha activado su habilidad especial al morir.");
	        }
	    }


	public void setTipo(String tipo) {
		this.tipo = tipo;

	}

	public void setDano(int dano) {
		this.dano = dano;

	}

	public void setMovimiento(int movimiento) {
		this.movimiento = movimiento;

	}
    public boolean estaMuerto() {
        return getSalud() <= 0;
    }

	public String getTipo() {

		return tipo;
	}

	public int getDano() {
		return dano;
	}

	public int getMovimiento() {
		return movimiento;
	}
	
	  public boolean esCorredor() {
	        return false;
	    }
	  public boolean esGordo() {
	        return false; 
	    }
	  public boolean esCaminante() {
	        return false; 
	    }
	  
    public abstract void habilidadEspecial();
    
    
}
