package zombice;

import java.util.ArrayList;
import java.util.Random;

public  class Arma {
	private String nombre;
	private int dano;
	private int alcance;
	private int acierto;
	
	
	public Arma(String nombre, int dano, int alcance, int acierto) {
		
		setNombre(nombre);
		setDano(dano);
		setAlcance(alcance);
		setAcierto(acierto);
	}
	
	public Arma() {
		
		setNombre("Daga");
		setDano(1);
		setAlcance(1);
		setAcierto(4);
    }

	 public static Arma getArma() {
	        Random random = new Random();
	        int probabilidad = random.nextInt(10); // 0-9 

	        
	        if (probabilidad < 2) { // 20% 
	            return new Arco("Arco Largo", 1, 2, 3);
	        } else if (probabilidad < 4) { // 20% 
	            return new Hacha("Hacha Doble", 2, 1, 3);
	        } else if (probabilidad < 7) { // 30% 
	            return new Hechizo("Bola de Fuego", 1, 3, 4);
	        } else { // 30% 
	            return new Espada("Espada Corta", 1, 1, 4);
	        }
	    }
	 
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setDano(int dano) {
		this.dano = dano;
	}
	
	public void setAlcance (int alcance) {
		this.alcance = alcance;
	} 
	
	public void setAcierto (int acierto) {
		this.acierto = acierto;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public int getDano() {
		return dano;
	}
	
	public int getAlcance() {
		return alcance;
	}
	
	public int getAcierto() {
		return acierto;
	}
	
	
	public void ataqueEspecial(ArrayList<Zombie> zombies) {
        System.out.println("No hay habilidad especial.");
    }
	

}
