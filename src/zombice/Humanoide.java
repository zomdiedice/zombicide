package zombice;

public  class Humanoide {
	private int saludMax;
	private int salud;
	private String nombre;
	private boolean vivo;
	
	public  Humanoide(int saludMax,int salud,String nombre) {
		setSaludMax(saludMax);
		setSalud(salud);
		setNombre(nombre);
		setVivo(vivo);
		
	}

	public void setVivo(boolean vivo) {
		vivo = true;
	}
	
	public boolean getVivo() {
		return vivo;	
	}

	public void setNombre(String nombre) {
		this.nombre=nombre;

	}
	public String getNombre() {
		return nombre;
	}

	public void setSalud(int salud) {
		this.salud=salud;

	}
	
	public int getSalud() {
		return salud;
	}

	public void setSaludMax(int saludMax) {
		this.saludMax=saludMax;
		
	}
	
	public int getSaludMax() {
		return salud;
	}
	
	public void recibirDanio(int dano) {
        salud -= dano;
        if (salud <= 0) {
            salud = 0;
            vivo = false; 
        }
	}

	
}
