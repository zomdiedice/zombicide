package zombice;

import java.util.ArrayList;
import java.util.Random;

public class Espada  extends Arma{

	public Espada(String nombre, int dano, int alcance, int acierto) {
		super(nombre, dano, alcance, acierto);
	}
	
	@Override
	public void ataqueEspecial(ArrayList<Zombie> zombies) {
        // Mata gratis a 2 zombis aleatorios
        Random random = new Random();
        for (int i = 0; i < 2; i++) {
            int index = random.nextInt(zombies.size());
            Zombie zombie = zombies.get(index);
            System.out.println("¡Ataque especial de la Espada! ¡El zombie '" + zombie.getNombre() + "' ha sido eliminado!");
            zombies.remove(index);
        }
    }
	
}
