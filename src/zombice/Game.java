package zombice;

import java.util.ArrayList;
import java.util.Scanner;

public class Game {
	public static ArrayList<Jugador> jugadores = new ArrayList<>();

	public static void main(String[] args) {
		Partida partida = new Partida();

		Scanner scanner = new Scanner(System.in);
		int opcion = -1;

		while (opcion != 0) {
			// Inicializamos la opción con un valor que no sea 0, 1 o 2
			System.out.println("\nMenú:");

			System.out.println("1- Nueva partida");
			System.out.println("2- Nuevo personaje");
			System.out.println("0- Salir");

			System.out.print("Seleccione una opción: ");
			opcion = scanner.nextInt();

			switch (opcion) {
			case 0:
				System.out.println("Saliendo del programa...");

				break;
			case 1:
				System.out.println("Iniciando nueva partida");
				crearNuevoPartida(partida);
				break;
			case 2:
				System.out.println("Creando nuevo personaje");

				crearNuevoPersonaje(partida);

				break;
			default:
				System.out.println(" no válida. Por favor, seleccione otro opción ");
				break;
			}
		}
	}

	private static void crearNuevoPartida(Partida partida) {
		Scanner leer = new Scanner(System.in);
		Arma Mandoble = new Arma("Mandoble", 2, 1, 4);
		Jugador james = new Jugador(7, 7, "James", Mandoble);
		Jugador marie = new Jugador(5, 5, "Marie", new Arma());
		Jugador jaci = new Jugador(5, 5, "Jaci",  new Arma());

		partida.agregarJugador(james);
		partida.agregarJugador(marie);
		partida.agregarJugador(jaci);

		ArrayList<Jugador> jugadoresSeleccionados = partida.seleccionarPersonajes();
		partida.setNivel(jugadoresSeleccionados.size());
		partida.jugarNivel(jugadoresSeleccionados);

	}

	private static void crearNuevoPersonaje(Partida partida) {
		Scanner leer = new Scanner(System.in);

		if (partida.jugadorMas10()) {

			return;
		} else {
			System.out.println("nombre de personaje");
			String nombre = leer.next();

			Arma arma = new Arma();

			Jugador nuevoJugador = new Jugador(5, 5, nombre, arma);
			partida.agregarJugador(nuevoJugador);
		}

	}

}
