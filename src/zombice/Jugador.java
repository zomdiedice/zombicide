package zombice;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Jugador extends Humanoide {
	private boolean nextnivel;

	private ArrayList<Arma> armasDisponibles;
    private Arma armaEquipada;

	public Jugador(int saludMax, int salud, String nombre ,Arma arma) {
		super(saludMax, salud, nombre);
		 armasDisponibles = new ArrayList<>();
	        armaEquipada = arma;
	        armasDisponibles.add(arma);// Las armas equipadas inicialmente se añaden a la lista de armas disponibles

	}


	  public void agregarArma(Arma arma) {
	        armasDisponibles.add(arma);
	    }

	    public void setArmaEquipada(Arma armaEquipada) {
	        this.armaEquipada = armaEquipada;
	    }

	    public Arma getArmaEquipada() {
	        return armaEquipada;
	    }
	    public boolean estaMuerto() {
	        return getSalud() <= 0;
	    }


	public boolean getArma() {
		return nextnivel;

	}

	
	public void setNextnivel(boolean nextnivel) {
		this.nextnivel = nextnivel;

	}

	public boolean getNextnivel() {
		return nextnivel;

	}
	
	 public void atacar(ArrayList<Zombie> zombies) {
		 if (armaEquipada != null) {
	        int alcance = armaEquipada.getAlcance();
	        Random random = new Random();
	        for (int i = 0; i < alcance; i++) {
	            
	            int randomIndex = random.nextInt(zombies.size());
	            Zombie zombie = zombies.get(randomIndex);
	            
	            
	            if (armaEquipada.getDano() >= zombie.getSalud()) {
	                // Comprueba si le has dado al zombi.
	                int dice = random.nextInt(6) + 1;
	                if (dice >= armaEquipada.getAcierto()) {
	                    System.out.println("¡Ataque exitoso! El zombie ha sido eliminado.");
	                    zombies.remove(zombie);
	                } else {
	                    System.out.println("¡Ataque fallido! El zombie esquivó el golpe.");
	                }
	                
	              
	                if (zombie.getSalud() == 0) {
	                    zombie.verificarActivacionHabilidadEspecial();
	                }
	            } else {
	                System.out.println("¡El arma no es lo suficientemente poderosa para eliminar al zombie!");
	            }
	        }
	    } else {
	        System.out.println("El jugador no tiene un arma equipada.");
	    }}





	 public void usarHabilidadEspecial(ArrayList<Zombie> zombies) {
	        // Comprobar si el jugador está equipado con un arma
	        if (armaEquipada == null) {
	            System.out.println(getNombre() + "No se equipan armas ni se pueden utilizar ataques especiales.");
	            return;
	        }
	        armaEquipada.ataqueEspecial(zombies);
	    }




	 public void buscar() {
	        Arma armaEncontrada = Arma.getArma();
	        System.out.println(getNombre() + " ha encontrado un arma: " + armaEncontrada.getNombre());
	        
	        System.out.println("Daño: " + armaEncontrada.getDano() + ", Alcance: " + armaEncontrada.getAlcance() + ", Acierto: " + armaEncontrada.getAcierto());
	        armasDisponibles.add(armaEncontrada);   
	 }





	  public void cambiarArma() {
	        Scanner scanner = new Scanner(System.in);

	        // Muestra una lista de las armas disponibles
	        System.out.println("Armas disponibles:");
	        for (int i = 0; i < armasDisponibles.size(); i++) {
	        System.out.println((i + 1) + ". " + armasDisponibles.get(i).getNombre());
	        }

	        // Pide al jugador que elija un arma
	        System.out.print("Seleccione el arma que desea equipar: ");
	        int seleccion = scanner.nextInt();

	        // Comprobar que la selección es válida
	        if (seleccion >= 1 && seleccion <= armasDisponibles.size()) {
	            Arma nuevaArma = armasDisponibles.get(seleccion - 1);
	            setArmaEquipada(nuevaArma);
	            System.out.println("Has equipado el arma: " + nuevaArma.getNombre());
	        } else {
	            System.out.println("Selección no válida. Inténtelo de nuevo.");
	        }
	    }
	  public void serAtacadoPorZombie(Zombie zombie) {
	        System.out.println(zombie.getNombre() + " ataca a " + getNombre() + "!");
	        int dano = zombie.getDano(); 
	        setSalud(getSalud() - dano); 
	        if (estaMuerto()) {
	            System.out.println(getNombre() + " ha sido derrotado por " + zombie.getNombre() + ".");
	        }
	    }
	  
}
