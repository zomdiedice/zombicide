package zombice;

import java.util.ArrayList;
import java.util.Random;

public class Hechizo  extends Arma {

	public Hechizo(String nombre, int dano, int alcance, int acierto) {
		super(nombre, dano, alcance, acierto);
	}

	
	 @Override
	    public void ataqueEspecial(ArrayList<Zombie> zombies) {
	       
	        Random random = new Random();
	        int zombiesEliminados = 0;
	        for (int i = 0; i < zombies.size(); i++) {
	            Zombie zombie = zombies.get(i);
	            if (zombie.esCaminante()) {
	                System.out.println("¡Ataque especial del Hechizo! ¡El Caminante '" + zombie.getNombre() + "' ha sido eliminado!");
	                zombies.remove(i);
	                zombiesEliminados++;
	                if (zombiesEliminados >= 2) {
	                    return; // Se han destruido suficientes caminantes para salir del ciclo.
	                }
	            }
	        }
	     
	        System.out.println("¡Ataque especial del Hechizo! No hay suficientes Caminantes para eliminar.");
	    }
}
