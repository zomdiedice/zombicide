package zombice;

public class Gordo extends Zombie {
   
	

	  public Gordo() {
	        super(2, 2, "Gordo", 1, 1, "Gordo");
	    }

	  @Override
	    public boolean esGordo() {
	        return true;
	    }
	@Override
    public void habilidadEspecial() {
        // Eliminar a otro gordo cuando muere
        ListaDeZombies.eliminarUnGordo();
    }
}