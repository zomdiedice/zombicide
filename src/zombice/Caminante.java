package zombice;

public class Caminante extends Zombie{

	
	
	public Caminante() {
		super(1, 1, "Caminante", 1, 1, "Caminante");
	}

	 @Override
	    public boolean esCaminante() {
	        return true;
	    }

	@Override
	public void habilidadEspecial() {
		
		ListaDeZombies.obtenerCantidadDeCaminantesVivos();
	}

}
