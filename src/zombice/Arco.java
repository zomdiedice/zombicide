package zombice;

import java.util.ArrayList;
import java.util.Random;

public class Arco  extends Arma{

	public Arco(String nombre, int dano, int alcance, int acierto) {
		super(nombre, dano, alcance, acierto);
	}
	
	@Override
    public void ataqueEspecial(ArrayList<Zombie> zombies) {
        
        Random random = new Random();
        for (int i = 0; i < zombies.size(); i++) {
            Zombie zombie = zombies.get(i);
            if (zombie.esCorredor()) {
                System.out.println("¡Ataque especial del Arco! ¡El Corredor '" + zombie.getNombre() + "' ha sido eliminado!");
                zombies.remove(i);
                return;
            }
        }
     // Si no hay corredores, emite el mensaje correspondiente
        System.out.println("¡Ataque especial del Arco! No hay Corredores para eliminar.");
    }
}
