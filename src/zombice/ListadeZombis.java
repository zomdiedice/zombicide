package zombice;

import java.util.ArrayList;
import java.util.List;

class ListaDeZombies {
    public static List<Zombie> zombiesVivos = new ArrayList<>();

    //agregar un zombie vivo a la lista
    public static void agregar(Zombie zombie) {
        zombiesVivos.add(zombie);
    }

    //obtener la cantidad de caminantes vivos
    public static int obtenerCantidadDeCaminantesVivos() {
        int cantidad = 0;

        for (int i = 0; i < zombiesVivos.size(); i++) {
        	  Zombie zombie = zombiesVivos.get(i);
            if ("Caminante".equals(zombie.getTipo())) {
                cantidad++;
            }
        }
        return cantidad;
    }
   
    //eliminar un gordo de la lista
    public static void eliminarUnGordo() {
        for (int i = 0; i < zombiesVivos.size(); i++) {
            Zombie zombie = zombiesVivos.get(i);
            if ("Gordo".equals(zombie.getTipo())) {
                zombiesVivos.remove(i);
                break; 
            }
        }
    }

    //eliminar todos los corredores vivos de la lista
    public static void eliminarCorredoresVivos() {
        for (int i = 0; i < zombiesVivos.size(); i++) {
            Zombie zombie = zombiesVivos.get(i);
            if ("Corredor".equals(zombie.getTipo())) {
                zombiesVivos.remove(i);
                i--; 
            }
        }
    }







}